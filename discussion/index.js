console.log("Hello world");


// Assignment Operators -----------------------------------------------
// addition assignment (+=)

let value = 8;
value += 15;
console.log(value);

// subtraction assignment (-=)

let subtract = 8;
subtract -= 5;
console.log(subtract);

// multiplication assignment (*=)
let multiply = 8;
multiply *= 2;
console.log(multiply);

// division assignment (/=)
let divide = 8;
divide /= 2;
console.log(divide);


// ARITHMETIC OPERATORS ---------------------------------------

// addition (+)

let x = 15;
let y = 10;

let sum = x + y;
console.log(sum);

// subtraction (-)

let a = 15;
let b = 10;

let difference = a - b;
console.log(difference);

// multiplication (*)

let c = 15;
let d = 10;

let product = c * d;
console.log(product);

// division (/)

let e = 15;
let f = 10;

let quotient = e / f;
console.log(quotient);

// remainder between the 2 values
// Modulus symbol '%'

let g = 15;
let h = 10;

let remainder = g % h;
console.log(remainder);
// answer is 5

// multiple operators ---------------------------------------
// it follows the PEMDAS Rule
// (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

// the operators were done in the following order

//1. 3 * 4 = 12
//2. 12 / 5 = 2.4
//3. 1 + 2 = 3
//4. 3 - 2.4 = 0.6

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// 1. 4 / 5 = 0.8 and 2 - 3 = -1
// 2. -1 * 0.8 = -0.8
// 3. 1 + -.08 = .2


// INCREMENT AND DECREMENT -------------------------------------

// increment (++)

// pre-increment (syntax ++variable)
let z = 1;
let preIncrement = ++z; // 1 + 1
console.log(preIncrement);

console.log(z);

// post-increment (syntax: variable++)
let postIncrement = z++;
console.log(postIncrement);

console.log(z);

// decrement (--)

// pre-decrement
let k = 8
let preDecrement = --k;
console.log(preDecrement);

console.log(k);

// post-decrement
let postDecrement = k--;
console.log(postDecrement);

console.log(k);

let bagongValue = 3;

let newValue = bagongValue++;
console.log(newValue);

console.log(bagongValue);


// TYPE COERCION -----------------------------------------------------

let numberA = 6;
let numberB = 'six';

let newNumber = numberA + numberB;
console.log(newNumber);

// adding number and boolean -------------------------------------
let expressionC = 10 + true;
console.log(expressionC);

let l = true;
console.log(typeof l);

let expressionE = 10 + false;
console.log(expressionE);

let expressionK = true + false;
console.log(expressionK);

// number with null value -----------------------------------------
let expressionG = 8 + null;
console.log(expressionG);

let expressionL = '8' + null;
console.log(expressionL);

//Conversion Rules:
 //1. If atleast one operand is an object, it will be converted into a primitive value/data type.
 //2. After conversion, if atleast 1 operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
 //3. In other cases where both operands are converted to numbers then an arithmetic operation is executed.

 // number with undefined ---------------------------------------

 let expressionZ = 9 + undefined;
 console.log(expressionZ); //NaN - it means Not a Number

// Comparisson operators --------------------------------------------------------

let name = 'Juan'

// Equality operator (==)
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true
console.log(1 == true); // true
console.log(1 == false); // false
console.log(name == 'Juan'); // true
console.log('juan' == name); // false - case sensitive to the value

// Inequality operator (!=) ---------------------------------------------

console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != '1'); // false
console.log(0 != false); // false
let juan = 'juan'
console.log('juan' != juan); // false

// STRICT EQUALITY OPERATORS (===) -------------------------------------------
// both value and data type must be equal

console.log(1 === 1); //true
console.log(1 === '1'); //false
console.log(0 === false); //false

// STRICK INEQUALITY OPERATORS (!==) ----------------------------------

console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== '1'); //true
console.log(0 !== false); //true

//Developer's tip: upon creating conditions or statement it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.

// Relational Operators ----------------------------------

let priceA = 1800;
let priceB = 1450;

console.log(priceA < priceB); //false
console.log(priceA > priceB); //true

let expressionI = 150 <= 150;
console.log(expressionI);

//Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. it is writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

// LOGICAL OPERATOR ---------------------------------------------

isLegalAge = true;
isRegistered = false;

// AND (&&)

let isallowedToVote = isLegalAge && isRegistered;
console.log('Is the person allowed to vote? ' + isallowedToVote);


// OR (||)

let isAllowedForVaccination = isLegalAge || isRegistered;
console.log(isAllowedForVaccination);


// NOT OPERATOR (! - exclamation point)

let isTaken = true;
let isTalented = false;

console.log(!isTaken);
console.log(!isTalented);